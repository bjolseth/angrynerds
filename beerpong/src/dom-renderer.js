const measure = (obj) => {
    const w = obj.bounds.max.x - obj.bounds.min.x;
    const h = obj.bounds.max.y - obj.bounds.min.y;
    return { w, h };
}


class DomRenderer {
  constructor(root, game) {
    this.root = root;
    this.game = game;
    this.objects = {};
  }

  createBall(ball) {
    const name = ball.data.name;
    const meta = this.game.gameState.players[name];
    const avatar = meta && meta.avatar;
    const el = div('ball');
    el.style.width = config.ballSize * 2 + 'px';
    el.style.height = config.ballSize * 2 + 'px';
    el.style.backgroundColor = meta.color;
    if (avatar) {
      el.style.backgroundImage = `url(${avatar})`;
    }
    this.objects[ball.id] = el;
    this.root.prepend(el); // appear behind cups etc
  }

  // we update each time step and create objects on the fly as needed
  // TODO use created events from matter instead
  updateBall(object) {
    const id = object.id;
    const el = this.objects[id]
    if (!el) this.createBall(object);
    else {
      const x = object.bounds.min.x;
      const y = object.bounds.min.y;
      // console.log(object);
      setPosAndAngle(el, x, y, object.angle);
    }
  }

  createGoal(object) {
    const goal = div('cup');
    this.objects[object.id] = goal;
    const x = object.bounds.min.x;
    const width = object.bounds.max.x - x;
    const y = object.bounds.min.y;
    const height = object.bounds.max.y - y;
    setPos(goal, x, y);
    setSize(goal, width, height);
    this.root.append(goal);
  }

  createObject(object) {
    const { label } = object;
    if (label === 'ball') this.createBall(object);
    else if (label === 'goal') this.createGoal(object);
  }

  update(gameObjects) {
    gameObjects.forEach(object => {
      if (!this.objects[object.id]) this.createObject(object);
      if (object.label === 'ball') this.updateBall(object);
    })
  }
}
