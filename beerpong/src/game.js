const {
	Engine, Bodies, World,  Render, Constraint, Vertices, MouseConstraint, Body,
	Events, Sleeping, Composite,
} = Matter;

function isCollision(bodyA, type1, bodyB, type2) {
	if (bodyA.label === type1 && bodyB.label === type2) return { bodyA, bodyB };
	else if (bodyA.label === type2 && bodyB.label === type1) return { bodyA: bodyB, bodyB: bodyA };
	return false;
}

function setColor(el, color) {
	const { render } = el;
	render.fillStyle = color;
	Body.set(el, 'render', render);
}

function absMax(val, max) {
	if (val < -max) return -max;
	if (val > max) return max;
	return val;
}

function createGoal(x, y, size, score) {
	const h = size * 2;
	const thick = config.goalThickness;
	const fillStyle = '#CF3C28';
	const red = { render: { fillStyle, lineWidth: 0 } };
	const left = Bodies.rectangle(x - size + 5, y, thick, h, red);
	const right = Bodies.rectangle(x + size - 5, y, thick, h, red);
	const bottom = Bodies.rectangle(x, y + h/2, size * 2 - thick, thick * 2, red);
	const sensorH = h/2;
	const sensor = Bodies.rectangle(x, y + sensorH / 2 - thick, size * 2 - thick * 2, sensorH, {
		label: 'goal-sensor',
		data: { score },
		isSensor: true,
		render: {
			fillStyle: '#ccc',
		},
	});

	const angle = config.goalSlant * Math.PI / 180;
	Body.rotate(left, -angle);
	Body.rotate(right, angle);
	const parts = [left, right, bottom, sensor];
	const goal = Body.create({
		parts,
		isStatic: true,
		label: 'goal',
	});
	return goal;
}

function createBall(x, y, r, color, data) {
	const props = {
		restitution: config.ballBounce,
		data,
		render: {
			fillStyle: color,
			strokeStyle: 'black',
			lineWidth: 1,
			// sprite: {
			// 	xScale: 0.2,
			// 	yScale: 0.2,
			// 	texture: avatar,
			// }
		},
		label: 'ball',
	};
	return Bodies.circle(x, y, r, props);
}

function rand(min, max) {
	return parseInt(min + Math.random() * (max - min));
}

class Game {

  constructor(world, engine, size, gameState) {
		this.gameState = gameState;
    this.playerBodies = {};
    this.size = size;
    this.world = world;
    this.setupCollisions(engine);
		this.step = 0;
  }

	getGameObjects() {
		return Composite.allBodies(this.world);
	}

	setCallback(callback) {
		this.callback = callback;
	}

	notify(type, data) {
		if (this.callback) this.callback(Object.assign({ type }, data));
	}

  launch(playerName, { x, y }) {
		const max = config.maxFlickSpeed;

		const speed = Math.min(Math.sqrt(x * x + y * y), max);
		const angle = Math.atan2(y, x);

		const x2 = speed * Math.cos(angle);
		const y2 = speed * Math.sin(angle);

		const velocity = { x: x2, y: y2 };
		// console.log(velocity);
  	const body = this.playerBodies[playerName];
  	if (!body) {
  		console.error('player not found:', playerName);
			addPlayer(playerName);
  		return;
  	}
		Sleeping.set(body, false);
  	Body.setVelocity(body, velocity);
  }

	getRandomStartPos() {
		return {
			x: rand(100, this.size.width),
			y: rand(this.size.height - 200, this.size.height - 100),
		};
	}

  addPlayer(info) {
		const { color, email } = info;
		const name = email;
		info.name = email;
		if (!this.playerBodies[name]) {
	    const ballSize = config.ballSize;
			info.color = randColor();
	    // todo dont overlap pos
	    const { x, y } = this.getRandomStartPos();
	    const player = createBall(x, y, ballSize, color, { name });
	    this.playerBodies[name] = player;
			this.gameState.scores[name] = 0;
			this.gameState.players[name] = info;
			Body.set(player, 'sleepThreshold', config.sleepThreshold);
	    World.add(this.world, player);
			Events.on(player, 'sleepStart', e => this.onPlayerSleep(e.source));
		}
		this.notify('playerAdded', info);
  }

	onPlayerSleep(player) {
		const name = player.data && player.data.name;
		if (player) {
			this.notify('playerSleep', { name });
		}
	}

  addGoal(x, y, size, score) {
    World.add(this.world, createGoal(x, y, size, score));
  }

	add(object, isStatic = true) {
		let obj;
		const { type, x, y, options = {}, fill } = object;
		options.isStatic = isStatic;
		options.render = {};
		if (fill) options.render.fillStyle = fill;

		if (type === 'rect') obj = Bodies.rectangle(x, y, object.width, object.height, options);
		else if (type === 'path') obj = Bodies.fromVertices(x, y, Vertices.fromPath(object.path), options);
		else {
			console.error('Unknown object type when creating level', type);
			return;
		}

		World.add(this.world, obj);
	}

  loadLevel(level) {
    const { goals, statics, nonstatics } = level;
    goals.forEach(({ x, y, size, score }) => this.addGoal(x, y, size, score));
		statics.forEach(obj => this.add(obj));
		nonstatics.forEach(obj => this.add(obj, false));
  }

	onGoalHit(player, goal) {
		const score = goal.data.score;
		const name = player.data.name;
		this.gameState.scores[name] += score;
		const newPos = this.getRandomStartPos();
		Body.setPosition(player, newPos);
		Body.setVelocity(player, { x: 0, y: 0 });
		this.notify('score', {
			score,
			totalScore: this.gameState.scores[player],
			player: name,
			position: goal.position,
		});
	}

	startDemo() {
		this.isDemo = !this.isDemo;
		const go = () => {
			const names = Object.keys(this.playerBodies);
			const player = names[rand(0, names.length)];
			this.launch(player, { x: rand(-15, 15), y: rand(-20, -38) });
			if (this.isDemo) {
				setTimeout(go, rand(100, 300));
			}
		};
		go();
	}

	notifyRethrows() {
		// TODO check speed and pos of each object, see if ok to rethrow
	}

	onStep(e) {
		this.step += 1;
		if (this.step % 60 === 0) this.notifyRethrows();
	}

  setupCollisions(engine) {
		const realThis = this; // matter.js hijacks this

		Events.on(engine, 'afterUpdate', this.onStep.bind(this));
    Events.on(engine, 'collisionStart', function(event) {
      const pairs = event.pairs;

      for (let i = 0; i < pairs.length; i++) {
        const { bodyA, bodyB } = pairs[i];
        const goal = isCollision(bodyA, 'goal-sensor', bodyB, 'ball');
        if (goal) {
					realThis.onGoalHit(bodyB, bodyA);
				}
      }
    });
  }

}
