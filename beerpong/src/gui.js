function formatName(email) {
  return email.split('@')[0].substr(0, 8);
}

function createHighScoreItem(item, number, forceFetchAvatar = false) {
  const playerData = context.game.gameState.players;
  const { color, avatar } = playerData[item.name] || {};
  const position = div("number", number < 10 ? '0' + number : number);
  const name = div("name", formatName(item.name));
  const score = div("score", item.score || '0');
  const ball = div('avatar');
  if (avatar) {
    ball.style.backgroundImage = `url(${avatar})`;
  }
  else if (forceFetchAvatar) {
    fetchWebexUser(item.name)
    .then(i => ball.style.backgroundImage = `url(${i.avatar})`)
    .catch(() => console.log('no avatar'));
  }
  ball.style.backgroundColor = color;
  const line = div("highscore-item");
  // line.appendChild(position);
  line.append(ball);
  line.appendChild(name);
  line.appendChild(score);
  return line;
}

function showGameDialog(topTen) {
  $('.overlay').style.display = 'flex';
  const el = $('.dialog .score-list');
  el.innerHTML = '';
  topTen.forEach((item, i) => {
    const line = createHighScoreItem(item, i + 1, true);
    el.appendChild(line);
  });
}

function updateScores(scores) {
  const list = Object.keys(scores).map(name => {
    return { name, score: scores[name] };
  });
  const sorted = list.sort((p1, p2) => p1.score > p2.score ? -1 : 1);

  const el = $('.sidebar .score-list');
  el.innerHTML = '';
  sorted.forEach((item, i) => {
    const line = createHighScoreItem(item, i + 1);
    el.appendChild(line);
  });
}

function updateTimeLeft(time) {
  const el = $('.time-left');
  const big = time < config.timeRemainingAlert;
  el.classList.toggle('highlight', big);
  el.innerText = 'Time: ' + time + ' s';
}

function showScore({ player, score, position }) {
  const label = div('score-toast', `${player} +${score}p`);
  setPos(label, position.x, position.y);
  const parent = $('.game-area');
  parent.appendChild(label);

  // todo listen to events
  setTimeout(() => parent.removeChild(label), 4000);
}

function updateGui(state) {
  updateScores(state.scores);
}
