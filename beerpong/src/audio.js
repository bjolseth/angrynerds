function playAudio(url) {
  const audio = new Audio(url);
  audio.play()
  .catch(e => console.log('not able to play audio', url));
}

function playBell() {
  playAudio('./assets/correct.wav');
}
