'use strict';

// main purpose of controller:
// initate setup of all components (server comm, game engine, game view, rest of gui)
// handle all comm btw server and clients, abstract away socket stuff
// persistence

const context = {
	game: null,
	theWire: null,
	renderer: null,
	isRunning: false,
	timer: 0,
};

function randColor() {
	const col = () => rand(50, 225);
	return `rgb(${col()}, ${col()}, ${col()})`;
}

function addRandomPlayers(world) {
	const avatars = [
		'tbjolset', 'peknudse', 'gistad', 'pirossi', 'mertsas',
		'marjorge', 'lbergers', 'andrhans', 'mbaisgar', 'kristisa', 'aleottes'
	];
	const double = avatars;//avatars.concat(avatars.map(a => a + '2'));
	while (double.length) {
		addPlayer(double.pop() + '@cisco.com');
	}
}

function setupWorld(canvas) {
	var engine = Engine.create({
		enableSleeping: true,
	});
	var world = engine.world;
	const size = getSize($('.main'));
	const game = new Game(world, engine, size, gameState);
	game.loadLevel(mainLevel);
	game.setCallback(onGameEvent);
	Engine.run(engine);

	if (config.debugMode) {
		setupDebugRenderer(engine, canvas, world);
	}
	context.game = game;
	context.renderer = new DomRenderer($('.game-area'), game);
	if (config.createMockPlayers) addRandomPlayers(game);
}

function onGameEvent(msg) {
	// console.log(msg);
	const { type } = msg;
	if (type === 'score') {
		playBell();
		showScore(msg);
	}

	updateGui(context.game.gameState);
	context.theWire.send(msg);
}

function addPlayer(email) {
	email = email.trim();
	return fetchWebexUser(email)
	.then((userInfo) => context.game.addPlayer(userInfo))
	.catch(() => context.game.addPlayer({ email }));
}

function onNetworkMessage(msg) {
	// console.log(msg);
	const game = context.game;
	switch(msg.type) {
		case 'launch':
			if (context.isRunning) game.launch(msg.name, { x: msg.vx, y: msg.vy }); break;
		case 'addPlayer':
			addPlayer(msg.player); break;
		default:
			console.error('Unknown network command', msg.type);
	}
}

function renderDom() {
	context.renderer.update(context.game.getGameObjects());
	window.requestAnimationFrame(renderDom);
}

function onKeyDown({ key }) {
	if (key === 'd') context.game.startDemo();
}

function showLink() {
	const url = location.origin + location.pathname + 'm.html';
	new QRCode($(".qr-code"), {
		text: url,
		width: 150,
		height: 150,
	});
	$('.footer').innerText = config.url;
}

function stopGame(saveScores = true) {
	context.isRunning = false;
	clearInterval(context.timer);
	if (saveScores) addLatestScores(context.game.gameState.scores);
	showGameDialog(getTopTen());
}

function startGame() {
	let time = config.gameTime;
	context.isRunning = true;
	context.timer = setInterval(() => {
		time -= 1;
		updateTimeLeft(time);
		if (time <= 0) {
			stopGame();
		}
	}, 1000);
}

function resetHighScore() {
 	localStorage.clear();
	location.reload();
}

function setListeners() {
	$('.start-game').ontouchend = () => {
		location.reload();
	}
	$('.qr-code-box').ontouchend = () => stopGame(false);
	$('.sidebar .heading').ondblclick = () => resetHighScore();
}

function init() {
	context.theWire = setupServerConnection(onNetworkMessage);
	var canvas = $('#world');
	setupWorld(canvas);
	renderDom();
	showLink();
	startGame();
	setListeners();
	window.onkeydown = onKeyDown;
}


window.onload = init;
