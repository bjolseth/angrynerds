const context = {
  allowLaunch: false,
  launchBounds: null,
  player: {},
  theWire: null,
  relaunchTimer: 0,
};

function showHome(home) {
  show($('.home'), home ? 'flex' : false);
  show($('.launcher'), !home ? 'flex' : false);
  if (home) {
    const name = context.player.name || getStoredName() || '@cisco.com';
    $('.input-name').value =  name;
  }
}

function movePlayer(dx, dy, animated) {
  const player = $('.player');
  player.classList.toggle('animated', animated);
  move(player, dx, dy);
}

function launchPlayer(vx, vy) {
  const k = 500;
  movePlayer(vx * k, vy * k, true);
  const r = v => parseInt(v * 100) / 100;
  $('.debug').innerText = `vx: ${r(vx)} vy: ${r(vy)}`;
  context.theWire.send({
    type: 'launch',
    name: context.player.name,
    vx: vx * config.mobileFlickBoost,
    vy: vy * config.mobileFlickBoost,
  });

  setEnableLaunch(false);
  context.relauuncTimer = setTimeout(() => setEnableLaunch(true), config.relaunchTime * 1000);
}

function onTouch(event) {
  if (!context.allowLaunch) return;
  const { type } = event;
  if (type === 'move' && context.allowLaunch) {
    movePlayer(event.dx, event.dy);
  }
  else if (type === 'release') {
    launchPlayer(event.vx, event.vy);
  }
}

function resetPlayer() {
  const player = $('.player');
  player.classList.remove('animated');
  const { width, height } = context.launchBounds;
  const s = window.getComputedStyle(player);
  const w = parseInt(s.getPropertyValue('width'));
  const h = parseInt(s.getPropertyValue('height'));
  player.style.left = ((width - w) / 2) + 'px';
  player.style.top = (height - h - 100) + 'px';
}

function preventIosBounce() {
  const stop = e => e.preventDefault();
  $('.launcher').addEventListener('touchmove', stop, { passive: false });
}

function setEnableLaunch(enable) {
  context.allowLaunch = enable;
  $('.player').classList.toggle('visible', enable);
  if (enable) {
    resetPlayer();
    clearTimeout(context.relaunchTimer);
  }
}

function setPlayer(name) {
  // context.player = data;
  // $('.player').style.backgroundColor = data.color;
  context.player = { name };
  $('.name').innerText = name;
}

function gotPlayerData(data) {
  const ball = $('.player');
  ball.style.backgroundColor = data.color;
  if (data.avatar) {
    const img = data.avatar.replace('~110', '~640');
    ball.style.backgroundImage = `url(${img})`;
  }
  else {
    ball.style.backgroundImage = '';
  }
}

function onNetworkMessage(msg) {
  // console.log('from server', msg);
  const { type } = msg;
  const isMe = msg.name === context.player.name;
  if (!isMe) return;

  switch (type) {
    case 'playerSleep': setEnableLaunch(true); break;
    case 'playerAdded': gotPlayerData(msg); break;
    // default: console.log('unknown msg', type);
  }
}

function setupTouch() {
  const pad = $('.launcher');
  const { x, y, width, height } = pad.getBoundingClientRect();
  context.launchBounds = { x, y, width, height };
  new TouchListener(pad, onTouch);
}

function getStoredName() {
  return localStorage.getItem('playerName');
}

function sendName() {
  const player = $('.input-name').value;
  if (!player) return;

  localStorage.setItem('playerName', player);
  context.theWire.send({ type: 'addPlayer', player });
  setPlayer(player);
  showHome(false);
}

function setupListeners() {
  $('button.name').onclick = (e) => {
    e.preventDefault();
    showHome(true);
  };
  $('.register-user').onclick = () => sendName();
  // $('.input-name').oninput = () => {
  //   const hasAlpha = $('.input-name').value.includes('@');
  //   show($('.default-domain'), !hasAlpha ? 'inline-block' : 'none');
  // }
}

function init() {
  context.theWire = setupServerConnection(onNetworkMessage);
  setupTouch();
  resetPlayer();
  setupServerConnection();
  preventIosBounce();
  setEnableLaunch(true);
  setupListeners();
  showHome(true);
}

window.onload = init;
