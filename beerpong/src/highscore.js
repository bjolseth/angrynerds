
function getAllScores() {
  const data = localStorage.getItem('all-scores');
  if (data) return JSON.parse(data);
  return {};
}

function getTopTen() {
  const scores = getAllScores();
  const list = Object.keys(scores).map(name => {
    return { name, score: scores[name] };
  });
  const sorted = list.sort((p1, p2) => p1.score > p2.score ? -1 : 1);
  return sorted.slice(0, 10);
}

function addLatestScores(scores) {
  const existing = getAllScores();
  for (const user in scores) {
    if (!existing[user] || scores[user] > existing[user]) {
      existing[user] = scores[user];
    }
  }
  localStorage.setItem('all-scores', JSON.stringify(existing));
}
