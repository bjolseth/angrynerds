Next in line:


- update time left
- save highscore, sort
- show highscore
- style game dialog


- Add happy hour
- Intro on mobile page
- Game state: started, running, stopped
- Display time left
- Button to start game, counter
- Sounds
- Flick: only use last samples
- Game: detect ball out of bounds, respawn
- Button to turn off audio, even just for development
- Qr code was very small might want to show large before start of game

Main screen

- Save scores to localstorage

Mobile

* Basic page scaffold
* Send start play message
- Fetch avatar
* Show mechanism shoot
* Listen to touch events, update graphic
* Send shoot message
* Receive msg ready to shoot again
- Receive score messages, display
- Token timed out message

Game design

* Tweak ball params
* Tweak launch params

Graphics

* Main game screen
* Background
* Baskets
* Avatars
* Score menu

Provisioning

* Host on prototypes.cisco.com
* shorty url
* QR code on screen (corners?)


The Extras

* Sound when scoring
- Music
- Demo when not being played
- Send Teams message after playing
- Sound when launching
- Sound when clash with another player
- Sound when hitting static object
- Animations: snow
- Wind, turbulence (indicate with snow or cisco flag)
- Achievements (two in a row, three in a row, all baskets hit, swish, multi-bounce, friend-bounce)
- Power ups (invisible to others, start from special start pos ground, adjust midair, see aim vector, double points per hit, faster rethrow time, smaller ball, choose where to start)
- Christmas music


Music:
yodel, ompa?
https://www.youtube.com/watch?v=yO7MWuJ7zLA
christmas Music
jokke?
please play responsibly and respect all local laws regarding drinking age
http://www.realmofdarkness.net/sb/angrybirds/
http://www.realmofdarkness.net//audio/vg/angrybirds/1.mp3
