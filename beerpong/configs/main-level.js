const W = 1920;
const H = 1030;
// wall size:
const W_S = 80;

const mainLevel = {
  goals: [
    { x: 150, y: 400, size: 33, score: 5000 },
    { x: 450, y: 200, size: 60, score: 2000 },
    { x: 300, y: 700, size: 60, score: 1500 },
    { x: 1000, y: 500, size: 100, score: 1000 },
    { x: 1500, y: 300, size: 40, score: 4000 },
    { x: 1700, y: 400, size: 60, score: 2000 },
  ],
  nonstatics: [
    // { type: 'rect', x: 600, y: 560, width: 250, height: 10, fill: 'red', label: 'lid' },
  ],
  statics: [
    { type: 'rect', x: W/2, y: H + W_S/2 - 3, width: W, height: W_S, fill: '#ccc', label: 'floor' },
    { type: 'rect', x: -W_S/2 + 3, y: H/2, width: W_S, height: H, fill: '#ccc', label: 'wall' },
    { type: 'rect', x: W + W_S/2 - 3, y: H/2, width: W_S, height: H, fill: '#ccc', label: 'wall' },
    { type: 'rect', x: W/2, y: 0 - W_S/2 + 3, width: W, height: W_S, fill: '#ccc', label: 'roof' },

    // { type: 'rect', x: 100, y: 530, width: 30, height: 800, fill: 'orange', label: 'gutter' },
    // { type: 'path', x: 40, y: H - 40, path: '0 0 0 100 100 100', fill: 'orange',label: 'corner' },
    // { type: 'path', x: W - 40, y: H - 40, path: '0 0 0 100 -100 100', fill: 'orange',label: 'gutter-nib' },
  ]
};
