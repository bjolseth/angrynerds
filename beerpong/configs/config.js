const config = {
  gameTime: 120,
  mobileFlickBoost: 18,
  ballSize: 20,
  ballBounce: 0.6,
  ballFriction: 0, // matter.js default: 0.1
  maxFlickSpeed: 40, // too high leads to ball skipping through walls
  goalHeight: 150,
  goalThickness: 20,
  goalSlant: 4, // angle of glass
  debugMode: false,
  createMockPlayers: false,
  relaunchTime: 2.0,
  timeRemainingAlert: 10,
  url: 'kyberheimen.com/angrynerds',
};
