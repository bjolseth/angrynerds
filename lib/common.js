// TODO support both
function setupServerConnection(onMsg) {
  const dev = location.href.includes('192.168') || location.href.includes('localhost');
  const localUrl = `ws://${location.hostname}:3000`;
  const production = 'wss://kyberheimen.com';
  const url = dev ? localUrl : production;
  return new TheWire(url, onMsg);
}
