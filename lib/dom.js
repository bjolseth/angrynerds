const $ = sel => document.querySelector(sel);

const $all = sel => Array.from(document.querySelectorAll(sel));

const div = (className, content) => {
    const div = document.createElement('div');
    div.className = className;
    div.innerText = content || '';
    return div;
};

const show = (el, visible = 'block') => {
  el.style.display = visible ? visible : 'none';
}

const setAngle = (el, rad) => {
  el.style.transform = `rotate(${rad}rad)`;
}

const setPos = (el, x, y) => {
  el.style.left = x + 'px';
  el.style.top = y + 'px';
}

const setPosAndAngle = (el, x, y, rad) => {
  el.style.transform = `translate3d(${x}px,${y}px,0) rotate(${rad}rad)`;
}

const setSize = (el, width, height) => {
  el.style.width = width + 'px';
  el.style.height = height + 'px';
}

const move = (el, dx, dy) => {
  const x = parseInt(el.style.left);
  const y = parseInt(el.style.top);
  setPos(el, x + dx, y + dy);
}

const getSize = (el) => {
  const b = el.getBoundingClientRect();
  return { x: b.x, y: b.y, width: b.width, height: b.height };
}
