class TheWire {

  constructor(host, callback) {
    if (!host) {
      throw new Error('must specify host');
    }
    this.host = host;
    this.callback = callback;
    this.connect();
  }

  connect() {
    console.log('Running The Wire broadcast service on', this.host);
    this.socket = new WebSocket(this.host);
    if (!this.callback) return;

    this.socket.onmessage = msg => this.onMessage(msg);
  }

  onMessage(e) {
    let msg;
    try {
      msg = JSON.parse(e.data).msg;
    }
    catch (e) {
      console.error('Not able to parse incoming socket message');
    }
    if (msg) {
      this.callback(msg);
    }
  }

  send(msg) {
    const state = this.socket.readyState;
    const OK = 1;
    if (state < 1) {
      this.socket.onopen = () => this.send(msg);
    }
    else if (state > OK) {
      this.connect();
      this.socket.onopen = () => this.send(msg);
    }
    else {
      const data = { msg, _wire_name: 'default' };
      this.socket.send(JSON.stringify(data));
    }
  }
}
