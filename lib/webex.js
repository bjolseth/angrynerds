
function fetchWebexUser(email) {
  // console.log('fetch', email);
  // allow certain local images for testing etc
  if (!email.includes('@')) {
    return Promise.resolve({
      displayName: email,
      avatar: `./avatars/${email}.jpg`,
      email,
    });
  }

  return findPhoto(email)
  .then(result => {
    if (result.items.length) {
      const user = result.items[0];
      const data = {};
      data.displayName = user.displayName;
      data.avatar = reduceSize(user.avatar);
      data.email = user.emails[0];
      return data;
    }
    return Promise.reject(new Error('User not found'));
  })
}

const url = 'https://api.ciscospark.com/v1/people/';
const msgUrl = 'https://api.ciscospark.com/v1/messages/';
const getRoomsUrl = 'https://api.ciscospark.com/v1/rooms?max=100';
const membershipUrl = 'https://api.ciscospark.com/v1/memberships';
const botToken = 'ZmIyY2JmODctZGZhZC00N2JhLWE0MWQtMTJhMTU4MDdhYWQ2ZTE1NTczMDQtMTY2';

const ValidUserIdLength = 36;

// valid sizes: 40, 50, 80, 110, 640 and 1600.
function reduceSize(url) {
  if (url) return url.replace('~1600', '~110');
}

function doIt(token, uri, method = 'GET', body = null) {
  const bearer = `Bearer ${token}`;
  /* global Headers */
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `Bearer ${token}`,
  }
  const payload = body ? { method, headers, body } : { method, headers };
  return fetch(uri, payload)
  .then(r => r.json())
  .catch(e => console.error(e));
}

function findPhoto(email) {
  const param = `?email=${email}&max=1`;
  const token = `Bearer ${botToken}`;

  const headers = new Headers();
  headers.append('Content-Type', 'application/json; charset=utf-8');
  headers.append('Authorization', token);
  return fetch(url + param, { method: 'GET', headers }).then( (r) => r.json());
}

function findUser(userId, token) {
  // hack for mock mode:
  if (userId.length < ValidUserIdLength) {
    const mock = { avatar: `./assets/${userId.toLowerCase()}.jpeg` };
    return new Promise(resolve => resolve(mock));
  }

  const uri = `${url}/${userId}`;
  return doIt(token, uri);
}

function sendMessage(userId, markdown, token) {
  console.log('Send spark msg:', userId, markdown.substr(0, 30) + '...');
  if (userId.length < ValidUserIdLength) return false;

  const body = JSON.stringify({ toPersonId: userId, markdown });
  return doIt(token, msgUrl, 'POST', body);
}

function sendMessageRoom(roomId, markdown, token) {
  console.log('Send spark msg:', roomId, markdown.substr(0, 30) + '...');

  const body = JSON.stringify({ roomId, markdown });
  return doIt(token, msgUrl, 'POST', body);
}

function sendMessageEmail(email, markdown, token) {
  console.log('send spark msg:', email, markdown.substr(0, 30) + '...');
  if (!email.includes('@')) return false;

  const body = JSON.stringify({ toPersonEmail: email, markdown });
  return doIt(token, msgUrl, 'POST', body);
}

function getRooms(token) {
  return doIt(token, getRoomsUrl);
}

function getMessages(roomId, token) {
  const uri = `${msgUrl}?roomId=${roomId}`;
  return doIt(token, uri);
}

function getPeople(roomId, token) {
  const uri = `${membershipUrl}?roomId=${roomId}&max=1000`;
  return doIt(token, uri);
}
