function setupDebugRenderer(engine, canvas, world) {
	var render = Matter.Render.create({
		engine,
		canvas,
		options: {
			width: 1920,
			height: 1080,
			background: 'transparent',
			wireframes: false,
			showSleeping: false,
			// showAngleIndicator: true
		}
	});
	Matter.Render.run(render);
	var mouseConstraint = Matter.MouseConstraint.create(engine, {
		element: canvas,
		constraint: {
			render: {
	        	visible: false
	    	},
	    	stiffness:0.8
	    }
	});
	Matter.World.add(world, mouseConstraint);
}
