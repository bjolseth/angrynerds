
const firstTouch = (e) => e.touches[0];
const getTime = () => new Date().getTime();

class TouchListener {

  constructor(element, callback) {
    if (!element || !callback) {
      throw new Error('Must specify element and callback');
    }
    this.callback = callback;
    element.ontouchstart = e => this.onTouchStart(e);
    element.ontouchmove = e => this.onTouchMove(e);
    element.ontouchend = e => this.onTouchEnd(e);
    this.prevX = 0;
    this.prevY = 0;
    this.startX = 0;
    this.startY = 0;
  }

  onTouchStart(e) {
    const { pageX, pageY } = firstTouch(e);
    this.prevX = pageX;
    this.prevY = pageY;
    this.startX = pageX;
    this.startY = pageY;
  }

  onTouchMove(e) {
    const { pageX, pageY } = firstTouch(e);
    const dx = pageX - this.prevX;
    const dy = pageY - this.prevY;
    this.callback({ type: 'move', dx, dy });

    const now = getTime();
    const time = now - this.lastTouch;
    this.lastTouch = now;
    this.vy = dy / time;
    this.vx = dx / time;

    this.prevX = pageX;
    this.prevY = pageY;
  }

  onTouchEnd(e) {
    this.callback({ type: 'release', vx: this.vx, vy: this.vy });
  }
}
